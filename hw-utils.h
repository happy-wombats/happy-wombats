/* hw-utils.h */

#ifndef _HW_UTILS_H
#define _HW_UTILS_H

#include <glib-object.h>
#include <clutter/clutter.h>
#include <clutter-box2d/clutter-box2d.h>

G_BEGIN_DECLS

typedef enum
{
  HW_JOINT_REVOLUTE,
  HW_JOINT_REVOLUTE2,
  HW_JOINT_DISTANCE,
  HW_JOINT_PRISMATIC
} HwJointType;

typedef struct
{
  gdouble length;
  gdouble frequency;
  gdouble damping;
} HwJointInfoDistance;

typedef struct
{
  gdouble min_length;
  gdouble max_length;
  ClutterVertex axis;
} HwJointInfoPrismatic;

typedef struct
{
  gchar             *name;
  ClutterBox2DJoint *joint;
  HwJointType        type;
  gchar             *actor1;
  gchar             *actor2;
  ClutterVertex      anchor1;
  ClutterVertex      anchor2;

  union {
    HwJointInfoDistance  distance;
    HwJointInfoPrismatic prismatic;
  };
} HwJoint;


void          hw_save (ClutterActor *box2d, const gchar *path, GList *joints);
ClutterActor *hw_load (const gchar *path, GList **joints);

void          hw_joint_free (HwJoint *joint, gboolean free);

void          hw_paint_offset (ClutterActor *actor, gint16 x, gint16 y);

ClutterActor *hw_utils_create_launcher (ClutterActor *scene,
                                        gfloat        x,
                                        guint         ammo);

void          hw_utils_explode (ClutterActor *scene,
                                gfloat        x,
                                gfloat        y,
                                gdouble       intensity);

void          hw_utils_play_sound (const gchar *filename,
                                   gdouble      x,
                                   gdouble      y,
                                   gdouble      volume);

void          hw_utils_play_music (const gchar *filename,
                                   gdouble      volume);

void          hw_utils_shutdown ();

G_END_DECLS

#endif /* _HW_UTILS_H */
