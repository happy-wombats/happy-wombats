/* hw-circle.h */

#ifndef _HW_CIRCLE_H
#define _HW_CIRCLE_H

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define HW_TYPE_CIRCLE hw_circle_get_type()

#define HW_CIRCLE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  HW_TYPE_CIRCLE, HwCircle))

#define HW_CIRCLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  HW_TYPE_CIRCLE, HwCircleClass))

#define HW_IS_CIRCLE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  HW_TYPE_CIRCLE))

#define HW_IS_CIRCLE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  HW_TYPE_CIRCLE))

#define HW_CIRCLE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  HW_TYPE_CIRCLE, HwCircleClass))

typedef struct _HwCircle HwCircle;
typedef struct _HwCircleClass HwCircleClass;
typedef struct _HwCirclePrivate HwCirclePrivate;

struct _HwCircle
{
  ClutterActor parent;

  HwCirclePrivate *priv;
};

struct _HwCircleClass
{
  ClutterActorClass parent_class;
};

GType hw_circle_get_type (void) G_GNUC_CONST;

ClutterActor *hw_circle_new (void);
ClutterActor *hw_circle_new_with_color (const ClutterColor *color);

void hw_circle_get_color (HwCircle *circle, ClutterColor *color);
void hw_circle_set_color (HwCircle *circle, const ClutterColor *color);

G_END_DECLS

#endif /* _HW_CIRCLE_H */
