
#include <string.h>
#include <mx/mx.h>
#include <clutter-box2d/clutter-box2d.h>
#include "hw-launcher.h"
#include "hw-scene.h"
#include "hw-utils.h"

typedef struct
{
  ClutterActor *box2d;
  guint         scroll_timeout;

  gboolean      started;
  ClutterActor *start_label;

  ClutterActor *score_box;
  ClutterActor *highscore_label;
  ClutterActor *score_label;
  gint          score;
  gboolean      complete;

  gchar        *level;

  ClutterActor *launcher;

  GKeyFile     *config;
} HwData;

static gboolean present_menu (HwData *data);


static void
get_launcher (HwData *data)
{
  data->launcher =
    clutter_container_find_child_by_name (CLUTTER_CONTAINER (data->box2d),
                                          "Launcher");
  if (!data->launcher)
    data->launcher = hw_utils_create_launcher (data->box2d, 50, 4);
}

static gboolean
demo_scroll_cb (HwData *data)
{
  gfloat destination;
  MxAdjustment *hadjust;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (data->box2d), &hadjust, NULL);

  destination = mx_adjustment_get_value (hadjust);
  if (destination > 10)
    destination = 0;
  else
    destination = mx_adjustment_get_upper (hadjust) -
                  mx_adjustment_get_page_size (hadjust);

  mx_adjustment_interpolate (hadjust, destination, 12000,
                             CLUTTER_EASE_IN_OUT_QUAD);

  return TRUE;
}

static gboolean
demo_scroll (HwData *data)
{
  MxAdjustment *hadjust;

  mx_scrollable_get_adjustments (MX_SCROLLABLE (data->box2d), &hadjust, NULL);

  demo_scroll_cb (data);
  data->scroll_timeout =
    g_timeout_add_seconds (12, (GSourceFunc)demo_scroll_cb, data);

  return FALSE;
}

static void
dingo_exploded_cb (HwScene *scene,
                   gfloat   x,
                   gfloat   y,
                   HwData  *data)
{
  gchar *text;

  data->score += 1000;
  text = g_strdup_printf ("Score: %06d", data->score);
  mx_label_set_text (MX_LABEL (data->score_label), text);
  g_free (text);
}

static void
level_complete_cb (HwScene *scene,
                   HwData  *data)
{
  gchar *text;
  gint extra_score;

  data->complete = TRUE;

  /* Add score for left-over ammo */
  extra_score = hw_launcher_get_ammo (HW_LAUNCHER (data->launcher)) * 500;

  text = g_strdup_printf ("Score: %06d +%d", data->score, extra_score);
  mx_label_set_text (MX_LABEL (data->score_label), text);
  g_free (text);

  data->score += extra_score;

  if (hw_launcher_get_ammo (HW_LAUNCHER (data->launcher)))
    g_timeout_add_seconds (5, (GSourceFunc)present_menu, data);
}

static void
notify_ammo_cb (HwLauncher *launcher,
                GParamSpec *pspec,
                HwData     *data)
{
  if (!hw_launcher_get_ammo (launcher))
    g_timeout_add_seconds (7, (GSourceFunc)present_menu, data);
}

static void
level_button_clicked_cb (ClutterActor *button,
                         HwData       *data)
{
  gint score;
  gchar *score_text;
  MxAdjustment *hadjust, *vadjust;

  ClutterActor *parent = clutter_actor_get_parent (data->box2d);

  data->level = g_strdup (g_object_get_data (G_OBJECT (button), "level"));

  g_source_remove (data->scroll_timeout);
  data->complete = FALSE;

  /* Load level */
  clutter_actor_destroy (data->box2d);
  data->box2d = hw_load (data->level, NULL);
  get_launcher (data);
  clutter_container_add_actor (CLUTTER_CONTAINER (parent), data->box2d);
  clutter_box2d_set_simulating (CLUTTER_BOX2D (data->box2d), TRUE);
  clutter_actor_grab_key_focus (data->launcher);

  /* Reset camera */
  mx_scrollable_get_adjustments (MX_SCROLLABLE (data->box2d),
                                 &hadjust, &vadjust);
  mx_adjustment_set_value (hadjust, 0);
  mx_adjustment_set_value (vadjust,
                           mx_adjustment_get_upper (vadjust) -
                           mx_adjustment_get_page_size (vadjust));

  /* Reset score and fade in score box */
  data->score = 0;
  mx_label_set_text (MX_LABEL (data->score_label), "Score: 000000");
  clutter_actor_animate (data->score_box, CLUTTER_EASE_OUT_QUAD, 250,
                         "opacity", 0xff, NULL);
  score = g_key_file_get_integer (data->config, data->level, "score", NULL);
  score_text = g_strdup_printf ("High score: %06d", score);
  mx_label_set_text (MX_LABEL (data->highscore_label), score_text);
  g_free (score_text);

  /* Connect to signals for scoring/game-over */
  g_signal_connect (data->box2d, "exploded::dingo",
                    G_CALLBACK (dingo_exploded_cb), data);
  g_signal_connect (data->box2d, "complete",
                    G_CALLBACK (level_complete_cb), data);
  g_signal_connect (data->launcher, "notify::ammo",
                    G_CALLBACK (notify_ammo_cb), data);
}

static gboolean
present_menu (HwData *data)
{
  GDir *dir;
  gboolean complete = TRUE;
  ClutterActor *dialog, *frame, *scrollview, *grid, *vbox, *label;

  /* Save high-score */
  if (data->complete && data->level)
    {
      gint score = g_key_file_get_integer (data->config, data->level,
                                           "score", NULL);

      if (data->score > score)
        g_key_file_set_integer (data->config, data->level,
                                "score", data->score);
      g_key_file_set_boolean (data->config, data->level, "complete", TRUE);
    }

  data->complete = FALSE;
  g_free (data->level);
  data->level = NULL;

  /* Fade out score box */
  clutter_actor_animate (data->score_box, CLUTTER_EASE_OUT_QUAD, 250,
                         "opacity", 0x00, NULL);

  /* Create level menu */
  dialog = mx_dialog_new ();
  mx_dialog_set_transient_parent (MX_DIALOG (dialog),
                                  clutter_actor_get_parent (data->box2d));

  frame = mx_frame_new ();
  mx_bin_set_child (MX_BIN (dialog), frame);
  mx_bin_set_fill (MX_BIN (dialog), TRUE, TRUE);

  grid = mx_grid_new ();
  mx_grid_set_homogenous_rows (MX_GRID (grid), TRUE);
  mx_grid_set_homogenous_columns (MX_GRID (grid), TRUE);
  mx_grid_set_row_spacing (MX_GRID (grid), 16);
  mx_grid_set_column_spacing (MX_GRID (grid), 16);

  scrollview = mx_scroll_view_new ();
  mx_scroll_view_set_scroll_policy (MX_SCROLL_VIEW (scrollview),
                                    MX_SCROLL_POLICY_VERTICAL);
  clutter_container_add_actor (CLUTTER_CONTAINER (scrollview), grid);

  vbox = mx_box_layout_new ();
  mx_stylable_set_style_class (MX_STYLABLE (vbox), "Level");
  mx_box_layout_set_orientation (MX_BOX_LAYOUT (vbox), MX_ORIENTATION_VERTICAL);

  label = mx_label_new_with_text ("Choose a level");
  mx_label_set_x_align (MX_LABEL (label), MX_ALIGN_MIDDLE);
  mx_stylable_set_style_class (MX_STYLABLE (label), "Title");
  clutter_container_add (CLUTTER_CONTAINER (vbox), label, scrollview, NULL);
  clutter_container_child_set (CLUTTER_CONTAINER (vbox), scrollview,
                               "expand", TRUE, NULL);

  mx_bin_set_child (MX_BIN (frame), vbox);
  mx_bin_set_fill (MX_BIN (frame), TRUE, TRUE);

  if ((dir = g_dir_open ("./", 0, NULL)))
    {
      GList *f, *files;
      const gchar *file;

      files = NULL;
      while ((file = g_dir_read_name (dir)))
        {
          if (!g_file_test (file, G_FILE_TEST_IS_REGULAR))
            continue;

          if (!g_str_has_suffix (file, ".ini"))
            continue;

          if (!g_str_has_prefix (file, "level"))
            continue;

          files = g_list_prepend (files, g_strdup (file));
        }
      g_dir_close (dir);

      files = g_list_sort (files, (GCompareFunc)strcmp);

      for (f = files; f; f = f->next)
        {
          gint score;
          gchar *level;
          gchar *score_text;
          ClutterActor *button;

          file = f->data;
          level = g_strdup (file + strlen ("level"));
          *(strchr (level, '.')) = '\0';
          button = mx_button_new_with_label (level);
          g_free (level);

          mx_stylable_set_style_class (MX_STYLABLE (button), "Level");

          g_object_set_data (G_OBJECT (button), "level", (gpointer)file);
          g_object_weak_ref (G_OBJECT (button),
                             (GWeakNotify)g_free,
                             (gpointer)file);

          clutter_container_add_actor (CLUTTER_CONTAINER (grid), button);

          g_signal_connect (button, "clicked",
                            G_CALLBACK (level_button_clicked_cb), data);
          g_signal_connect_swapped (button, "clicked",
                                    G_CALLBACK (clutter_actor_hide),
                                    dialog);

          /* Set high-score as tool-tip text */
          score = g_key_file_get_integer (data->config, file, "score", NULL);
          score_text = g_strdup_printf ("High score: %06d", score);
          mx_widget_set_tooltip_text (MX_WIDGET (button), score_text);
          g_free (score_text);

          mx_widget_set_disabled (MX_WIDGET (button), !complete);

          complete = g_key_file_get_boolean (data->config, file,
                                             "complete", NULL);
        }
      g_list_free (files);
    }

  clutter_actor_show (dialog);
  g_signal_connect_after (dialog, "notify::mapped",
                          G_CALLBACK (clutter_actor_destroy), NULL);

  return FALSE;
}

static gboolean
captured_event_cb (ClutterActor *actor, ClutterEvent *event, HwData *data)
{
  if (data->started)
    return FALSE;

  if ((event->type == CLUTTER_BUTTON_PRESS) ||
      (event->type == CLUTTER_KEY_PRESS))
    {
      data->started = TRUE;
      present_menu (data);

      clutter_actor_animate (data->start_label, CLUTTER_EASE_OUT_QUAD, 250,
                             "opacity", 0x00, NULL);

      return TRUE;
    }

  return TRUE;
}

int
main (int argc, char **argv)
{
  MxWindow *window;
  MxToolbar *toolbar;
  MxApplication *app;
  gchar *config_data, *file;
  ClutterLayoutManager *manager;
  ClutterActor *box, *stage, *scroll, *logo;

  HwData data = { 0, };

  app = mx_application_new (&argc, &argv, "Happy Wombats",
                            MX_APPLICATION_SINGLE_INSTANCE);
  window = mx_application_create_window (app);
  stage = (ClutterActor *)mx_window_get_clutter_stage (window);

  /* Load style */
  mx_style_load_from_file (mx_style_get_default (), "hw.css", NULL);

  scroll = mx_kinetic_scroll_view_new ();
  mx_kinetic_scroll_view_set_deceleration (MX_KINETIC_SCROLL_VIEW (scroll), 5);

  /* Load demo level */
  data.box2d = hw_load ("demo.ini", NULL);

  clutter_container_add_actor (CLUTTER_CONTAINER (scroll), data.box2d);

  /* Add start label */
  data.start_label = mx_label_new_with_text ("Click to start");
  mx_stylable_set_style_class (MX_STYLABLE (data.start_label), "Start");

  /* Create score box */
  data.score_box = mx_box_layout_new ();
  mx_stylable_set_style_class (MX_STYLABLE (data.score_box), "Score");

  logo = clutter_texture_new_from_file ("logo-small.png", NULL);
  data.highscore_label = mx_label_new ();
  data.score_label = mx_label_new ();
  mx_label_set_x_align (MX_LABEL (data.highscore_label), MX_ALIGN_END);
  mx_label_set_x_align (MX_LABEL (data.score_label), MX_ALIGN_END);
  mx_label_set_y_align (MX_LABEL (data.highscore_label), MX_ALIGN_MIDDLE);
  mx_label_set_y_align (MX_LABEL (data.score_label), MX_ALIGN_MIDDLE);
  clutter_container_add (CLUTTER_CONTAINER (data.score_box),
                         logo,
                         data.highscore_label,
                         data.score_label,
                         NULL);
  clutter_container_child_set (CLUTTER_CONTAINER (data.score_box), logo,
                               "y-fill", FALSE,
                               "x-fill", FALSE,
                               "x-align", MX_ALIGN_START,
                               "expand", TRUE, NULL);
  clutter_actor_set_opacity (data.score_box, 0x00);

  /* Add bits to the toolbar */
  toolbar = mx_window_get_toolbar (window);
  mx_bin_set_fill (MX_BIN (toolbar), TRUE, FALSE);
  clutter_container_add_actor (CLUTTER_CONTAINER (toolbar), data.score_box);

  /* Add everything to the window */
  manager = clutter_bin_layout_new (CLUTTER_BIN_ALIGNMENT_FILL,
                                    CLUTTER_BIN_ALIGNMENT_FILL);
  box = clutter_box_new (manager);
  clutter_container_add (CLUTTER_CONTAINER (box),
                         scroll, data.start_label, NULL);
  clutter_bin_layout_set_alignment (CLUTTER_BIN_LAYOUT (manager),
                                    data.start_label,
                                    CLUTTER_BIN_ALIGNMENT_END,
                                    CLUTTER_BIN_ALIGNMENT_END);
  clutter_bin_layout_set_alignment (CLUTTER_BIN_LAYOUT (manager),
                                    data.score_box,
                                    CLUTTER_BIN_ALIGNMENT_END,
                                    CLUTTER_BIN_ALIGNMENT_START);

  mx_window_set_child (window, box);

  /* Connect to captured-event signal to start game */
  g_signal_connect (scroll, "captured-event",
                    G_CALLBACK (captured_event_cb), &data);
  clutter_actor_grab_key_focus (scroll);

  /* Load configuration */
  data.config = g_key_file_new ();
  file = g_build_filename (g_get_user_config_dir (), "happy-wombats", NULL);
  g_mkdir_with_parents (file, 0755);
  g_free (file);
  file = g_build_filename (g_get_user_config_dir (),
                           "happy-wombats", "config.ini", NULL);
  g_key_file_load_from_file (data.config, file, G_KEY_FILE_NONE, NULL);

  /* Start simulation */
  clutter_box2d_set_simulating (CLUTTER_BOX2D (data.box2d), TRUE);
  clutter_actor_set_size (stage, 800, 600);
  clutter_actor_show (stage);

  hw_utils_play_music ("01 Happy Wombat Intro.ogg", 1.0);

  g_timeout_add (100, (GSourceFunc)demo_scroll, &data);
  mx_application_run (app);

  /* Save configuration */
  config_data = g_key_file_to_data (data.config, NULL, NULL);
  g_file_set_contents (file, config_data, -1, NULL);
  g_free (config_data);
  g_free (file);
  g_key_file_free (data.config);

  hw_utils_shutdown ();

  return 0;
}
