
default: hw hw-editor

clean:
	rm -f hw hw-editor hw-marshal.[ch]

H_SRCS = \
	 hw-circle.h \
	 hw-launcher.h \
	 hw-marshal.h \
	 hw-scene.h \
	 hw-utils.h

C_SRCS = \
	 hw-circle.c \
	 hw-launcher.c \
	 hw-marshal.c \
	 hw-scene.c \
	 hw-utils.c

CFLAGS = -Wall -g -O0 `pkg-config --cflags mx-1.0 clutter-box2d-0.12 libcanberra`
LIBS = `pkg-config --libs mx-1.0 clutter-box2d-0.12 libcanberra` -lm

hw-marshal.c: hw-marshal.list
	glib-genmarshal --body --prefix=hw_marshal hw-marshal.list > hw-marshal.c

hw-marshal.h: hw-marshal.list
	glib-genmarshal --header --prefix=hw_marshal hw-marshal.list > hw-marshal.h

hw: hw-main.c $(C_SRCS) $(H_SRCS)
	$(CC) -o hw hw-main.c $(C_SRCS) $(CFLAGS) $(LIBS)

hw-editor: hw-editor.c $(C_SRCS) $(H_SRCS)
	$(CC) -o hw-editor hw-editor.c $(C_SRCS) $(CFLAGS) $(LIBS)

